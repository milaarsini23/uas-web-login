<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Hello <?= $user['name']; ?></h1>

    <div class="row">
        <div class="col">

                <?= form_error('message', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                <?= $this->session->flashdata('message'); ?>

                <a href="" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Add Message</a>

                <table class="table table-bordered text-center table-hover table-dark mt-3">
                    <thead>
                        <tr>
                        <th scope="col">Date</th>
                        <th scope="col">Sender</th>
                        <th scope="col">Message</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($message as $msg) : ?>
                        <tr>
                            <th><?= $msg['datetime']; ?></th>
                            <th><?= $user['name']; ?></th>
                            <td><?= $msg['message']; ?></td>
                        </tr>
                        <?php endforeach;  ?>
                    </tbody>
                    </table>
            </div>
        </div>
    </div>
                        
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Send Message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="<?= base_url('user'); ?>" method="post">
          <div class="modal-body">
            <div class="form-group">
                <input type="text" class="form-control" id="message" name="message" placeholder="Message">
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Add</button>
          </div>
      </form>
    </div>
  </div>
</div>


<div>
